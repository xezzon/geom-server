package indi.xezzon.geom.common.exception;

import indi.xezzon.geom.common.model.IResultCode;

/**
 * @author xezzon
 */
public class NormalServerException extends AbstractNormalException {
    public NormalServerException(IResultCode resultCode) {
        super(resultCode);
    }

    public NormalServerException(IResultCode resultCode, Throwable cause) {
        super(resultCode, cause);
    }
}
