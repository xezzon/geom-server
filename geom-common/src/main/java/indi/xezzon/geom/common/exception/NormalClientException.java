package indi.xezzon.geom.common.exception;

import indi.xezzon.geom.common.model.IResultCode;

/**
 * @author xezzon
 */
public class NormalClientException extends AbstractNormalException {
    public NormalClientException(IResultCode resultCode) {
        super(resultCode);
    }

    public NormalClientException(IResultCode resultCode, Throwable cause) {
        super(resultCode, cause);
    }
}
