package indi.xezzon.geom.common.exception;

import indi.xezzon.geom.common.model.IResultCode;

/**
 * @author xezzon
 */
public class AbstractNormalException extends RuntimeException {
    private final String code;

    public AbstractNormalException(IResultCode resultCode) {
        super(resultCode.getMessage());
        this.code = resultCode.getCode();
    }

    public AbstractNormalException(IResultCode resultCode, Throwable cause) {
        super(resultCode.getMessage(), cause);
        this.code = resultCode.getCode();
    }

    public String getCode() {
        return this.code;
    }
}
