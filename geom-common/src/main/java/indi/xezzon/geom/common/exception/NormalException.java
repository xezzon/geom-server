package indi.xezzon.geom.common.exception;

/**
 * @author xezzon
 */
public class NormalException extends RuntimeException {
    public NormalException(String message) {
        super(message);
    }

    public NormalException(String message, Throwable cause) {
        super(message, cause);
    }
}
