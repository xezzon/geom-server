package indi.xezzon.geom.common.exception;

import indi.xezzon.geom.common.model.IResultCode;

/**
 * @author xezzon
 */
public class NormalThirdPartyException extends AbstractNormalException {
    public NormalThirdPartyException(IResultCode resultCode) {
        super(resultCode);
    }

    public NormalThirdPartyException(IResultCode resultCode, Throwable cause) {
        super(resultCode, cause);
    }
}
