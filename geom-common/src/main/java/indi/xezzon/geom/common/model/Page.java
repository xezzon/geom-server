package indi.xezzon.geom.common.model;

import java.util.List;

/**
 * @author xezzon
 */
public class Page<T> {
    private int pageNum;
    private int pageSize;
    private List<T> records;
    private int total;

    public Page() {}

    public Page(int pageNum, int pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public Page<T> setPageNum(int pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public int getPageSize() {
        return pageSize;
    }

    public Page<T> setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public List<T> getRecords() {
        return records;
    }

    public Page<T> setRecords(List<T> records) {
        this.records = records;
        return this;
    }

    public int getTotal() {
        return total;
    }

    public Page<T> setTotal(int total) {
        this.total = total;
        return this;
    }
}
