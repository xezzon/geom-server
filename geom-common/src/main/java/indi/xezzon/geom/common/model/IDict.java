package indi.xezzon.geom.common.model;

/**
 * @author xezzon
 */
public interface IDict {
    /**
     * 获取字典目
     * @return 字典目
     */
    String getTag();

    /**
     * 获取字典值
     * @return 字典值
     */
    String getCode();

    /**
     * 获取字典名
     * @return 字典名
     */
    String getDesc();
}
