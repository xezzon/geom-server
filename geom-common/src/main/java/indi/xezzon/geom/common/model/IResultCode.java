package indi.xezzon.geom.common.model;

/**
 * @author xezzon
 */
public interface IResultCode {
    /**
     * 返回错误码
     * @return 错误码
     */
    String getCode();

    /**
     * 返回错误描述
     * @return 错误描述
     */
    String getMessage();
}
