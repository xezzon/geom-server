package indi.xezzon.geom.common.model;

import java.beans.JavaBean;
import java.io.Serializable;
import java.util.Objects;

/**
 * 字典
 * @author xezzon
 */
@JavaBean
public final class Dict implements Serializable, IDict {
    private String tag;
    private String code;
    private String desc;

    @Override
    public String getTag() {
        return tag;
    }

    public Dict setTag(String tag) {
        this.tag = tag;
        return this;
    }

    @Override
    public String getCode() {
        return code;
    }

    public Dict setCode(String code) {
        this.code = code;
        return this;
    }

    @Override
    public String getDesc() {
        return desc;
    }

    public Dict setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Dict dict = (Dict) o;
        return tag.equals(dict.tag) && code.equals(dict.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tag, code);
    }
}
